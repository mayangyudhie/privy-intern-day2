// //let, const
// let size
// size = 11;
// console.log(size);

// const score = 10;
// console.log(score);

// // String, Number, Bolean, null, underfined
// const name = 'Astara';
// const age = 24;
// const rating = 5;
// const isCool = "true";
// const x = null;
// const y = undefined;
// let z;

// console.log(typeof x);

// const nama = 'Mayang';
// const umur = 24;

// // Concatenation (Penggabungan String)
// console.log('My name is ' + nama + ' and I am ' + umur);

// // Template String
// console.log(`My name is ${nama} and I am ${umur}`);

// const s = 'Hallo Everybody';

//  //untuk menghitung jumlah panjang karakter yang ada pada variabel s
// console.log(s.length); 

// //mengatur supaya menjadi hurup kecil semua
// console.log(s.toLowerCase()); 

// //mengatur hurup besar/kapital semua
// console.log(s.toLocaleUpperCase()); 

// //substring berguna untuk memisahkan string sesuai dengan parameter yang dimasukkan, agar menjadi array
// console.log(s.substring(0, 5)); 

// //string yang sudah terpotong nantinya akan menjadi huruf kapital
// console.log(s.substring(0, 5).toLocaleUpperCase());

// //untuk membagi string ke array string
// console.log(s.split(''));

// //Aarays -variables that hold multiple values
// const numbers = new Array(1,2,3,4,5);

// console.log(numbers);

// const fruits = ['apples', 'grape', 'pears'];
// //memasukkan value ke sebuah array disimpan di paling akhir atau dipaling kanan
// fruits.push('mangos');

// //memasukkan value ke sebuah array disimpan di paling awal atau ujung kiri
// fruits.unshift('strawberries');

// //untuk menghapus / remove isi array paling akhir.
// fruits.pop();

// //memeriksa apakah nilai yang diteruskan adalah Array (jika iya = true, jika salah=false)
// console.log(Array.isArray('hello'));

// //mengembalikan posisi nilai string dari kejadian pertama dari string yang ditentukan.
// //Jika tidak ada pencocokan string ditemukan -1 dikembalikan
// console.log(fruits.indexOf('oranges'));

// console.log(fruits);

// //indeks array dimulai dari angka 0 bukan angka 1
// console.log(fruits[0]); 

// const person = {
//     firstName: 'Gading',
//     lastName: 'Yudhiastara',
//     age: 17,
//     hobbies: ['music', 'movie', 'sport'],
//     addres: {
//         street: 'Jalan Jambu',
//         city: 'Bali',
//         state: 'Indonesia'
//     }
// }
// //menunjukkan hobbies pada array ke 1 
// console.log(person.hobbies[1]);

// //menunjukkan alamat kota
// console.log(person.addres.city);

// //menambahkan email
// person.email = 'mayangyudhie@gmail.com';
// console.log(person);

// const todos = [
//     {
//         id: 1,
//         text: 'Take out trash',
//         isCompleted: true
//     },
//     {
//         id: 2,
//         text: 'Meeting with boss',
//         isCompleted: true
//     },
//     {
//         id: 3,
//         text: 'Dentist appt',
//         isCompleted: false
//     }
// ];
// //menampilkan object yang dipilih
// console.log(todos[1].text);

// //for (perulangan di setiap perulangan i akan bertambah +1 (i++))
// for(let i = 0; i <= 10; i++) {
//     console.log(`While Loop Number: ${i}`);
// }
// //while (Pengulangan suatu variabel berdasarkan suatu kondisi, dengan pemeriksaan kondisi yang dilakukan di awal pengulangan)
// let i = 0;
// while(i < 10) {
//     console.log(`While Loop Number: ${i}`);
//     i++;
// }
// for(let i = 0; i < todos.length; i++) {
//     console.log(todos[i].text);
// }
// for(let todo of todos) {
//     console.log(todo.id);
// }
// //untuk melakukan perulangan dari jumlah element array
// todos.forEach(function(todo) {
//     console.log(todo.text);
// })
// //map (iterasi objek dalam suatu array)
//  const todoText = todos.map(function(todo) {
//     return todo.text;
//  })
//  console.log(todoText);
//  //filter (untuk mencari semua elemen di dalam array yang sesuai dengan kriteria tertentu)
//  const todoCompleted = todos.filter(function(todo) {
//     return todo.isCompleted === true;
//  }).map(function(todo){
//     return todo.text;
//  })
//  console.log(todoCompleted);

//  const a = 10;

//  if(a === 10) {
//     console.log('a is 10');
//  } else {
//     console.log('a is NOT 10');
//  }
//  const e = 4;
//  const f = 10;

//  if(e === 10) {
//     console.log('e is 10');
//  } else if(x > 10) {
//     console.log('e is greater than 10');
//  } else {
//     console.log('e is less than 10');
//  }

//  const ee = 4;
//  const ff = 11;

//  if(ee > 5 || y > 10) {
//     console.log('ee is more than 5 or ff is more than 10');
//  } 

//  const xx = 6;
//  const yy = 11;

//  if(xx > 5 && yy > 10) {
//     console.log('xx is more than 5 or yy is more than 10');
//  } 

//  if(x > 5) {
//     if(y > 10){

//     }
//  }

//  const ax = 9;

//  const color = 'green';

//  switch(color) {
//     case 'red' :
//         console.log('color is red');
//         break;
//         case 'blue' :
//         console.log('color is blue');
//         break;
//         default:
//         console.log('color is NOT red or blue');
//         break;
//  }
// //
//  function addNums (num1, num2) {
//     console.log(num1 + num2);
//  }
// addNums(5,4);
// //
// function addNumm (num1, num2) {
//     console.log(num1 + num2);
//  }
// addNumm ();
// //
// function addNuum (num1 = 1, num2 = 1) {
//     console.log(num1 + num2);
//  }
// addNuum ();
// //
// function addNuum (num1 = 1, num2 = 1) {
//     console.log(num1 + num2);
//  }
// addNuum (5, 5);
// //
// function addNum11 (num1 = 1, num2 = 1) {
//     return num1 + num2;
//  }
// console.log(addNum11(5, 5));
// //
// const addNamb = (num1 = 1, num2 = 1) => num1 + num2;

// console.log(addNamb(5, 5));

// //
// const addNambe = num1 => num1 + 5;
// console.log(addNambe(5));

// // Constructor Function
// function Personn(firstName, lastName, dob) {
//     // Set object properties
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob = new Date(dob); // Set to actual date object using Date constructor
//     // this.getBirthYear = function(){
//     //   return this.dob.getFullYear();
//     // }
//     // this.getFullName = function() {
//     //   return `${this.firstName} ${this.lastName}`
//     // }
//   }
  
//   // Instantiate an object from the class
//   const personn1 = new Personn('John', 'Doe', '7-8-80');
//   const personn2 = new Personn('Steve', 'Smith', '8-2-90');
  
//   console.log(personn2);
  
//   // console.log(person1.getBirthYear());
//   // console.log(person1.getFullName());

  
//   // Built in constructors
//   const namaa = new String('Kevin');
//   console.log(typeof namaa); // Shows 'Object'
//   const numms = new Number(5);
//   console.log(typeof numms); // Shows 'Object'
  
  
//   class Person {
//     constructor(firstName, lastName, dob) {
//       this.firstName = firstName;
//       this.lastName = lastName;
//       this.dob = new Date(dob);
//     }
  
//     // Get Birth Year
//     getBirthYear() {
//       return this.dob.getFullYear();
//     }
  
//     // Get Full Name
//     getFullName() {
//       return `${this.firstName} ${this.lastName}`
//     }
//   }

//   // Get Birth Year
//   Person.prototype.getBirthYear = function () {
//    return this.dob.getFullYear();
//  }
 
//  // Get Full Name
//  Person.prototype.getFullName = function() {
//    return `${this.firstName} ${this.lastName}`
//  }
  
//   const person1 = new Person('John', 'Doe', '7-8-80');
//   console.log(person1.getBirthYear());

// // ELEMENT SELECTORS

// // Single Element Selectors
// console.log(document.getElementById('my-form'));
// console.log(document.querySelector('.container'));
// // Multiple Element Selectors
// console.log(document.querySelectorAll('.item'));
// console.log(document.getElementsByTagName('li'));
// console.log(document.getElementsByClassName('item'));

// const items = document.querySelectorAll('.item');
// items.forEach((item) => console.log(item));


// // 
// const ul = document.querySelector('.items');
// // ul.remove();
// // ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'Hello';
// ul.children[1].innerText = 'Brad';
// ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

// const btn = document.querySelector('.btn');
// // btn.style.background = 'red';


// // 
// btn.addEventListener('click', e => {
//   e.preventDefault();
//   console.log(e.target.className);
//   //document.getElementById('my-form').style.background = '#ccc';
//   //document.querySelector('body').classList.add('bg-dark');
//   ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
// });

// // 
// const namesInput = document.querySelector('#name');
// namesInput.addEventListener('input', e => {
//   document.querySelector('.container').append(nameInput.value);
// });

// 
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// 
myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
  e.preventDefault();
  
  if(nameInput.value === '' || emailInput.value === '') {
    // alert('Please enter all fields');
    msg.classList.add('error');
    msg.innerHTML = 'Please enter all fields';

    // 
    setTimeout(() => msg.remove(), 3000);
  } else {
    // 
    const li = document.createElement('li');

    // 
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    
    li.innerHTML = `<strong>${nameInput.value}</strong>: ${emailInput.value}`;

    
    userList.appendChild(li);

    // Clear fields
    nameInput.value = '';
    emailInput.value = '';
  }
}